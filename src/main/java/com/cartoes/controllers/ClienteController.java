package com.cartoes.controllers;

import com.cartoes.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cartoes.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clientService;

    @PostMapping
    Cliente CadastraCliente(@RequestBody Cliente pcliente) {
        return clientService.CadastraCliente(pcliente);
    }

    @GetMapping
    Iterable<Cliente> ConsultarTodosClientes() {
        return clientService.ConsultarClientes();
    }

    @GetMapping("/{id}")
    Cliente ConsultarPorId(@PathVariable(name = "id") int idCliente) {
        try {
            return clientService.ConsultarClientePorId(idCliente);
        } catch (RuntimeException ex) {
            throw new RuntimeException(ex.getMessage());
        }

    }


}