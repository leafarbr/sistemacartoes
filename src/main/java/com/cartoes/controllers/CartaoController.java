package com.cartoes.controllers;

import com.cartoes.models.Cartao;
import com.cartoes.models.Cliente;
import com.cartoes.models.DTOs.CartaoDTOEntrada;
import com.cartoes.models.DTOs.CartaoDTOEntradaStatus;
import com.cartoes.services.CartaoService;
import com.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    Cartao CadastrarCartao(@RequestBody CartaoDTOEntrada cartao)
    {
        try {
            Cliente objCliente = clienteService.ConsultarClientePorId(cartao.getClientId());

            Cartao objCartao = new Cartao(cartao.getNumero(), objCliente, false);

            return cartaoService.CadastraCartao(objCartao);
        }
        catch (RuntimeException ex)
        {
            throw new RuntimeException(ex.getMessage());
        }

    }

    @GetMapping("/{numero}")
    Cartao ConsultarCartao(@PathVariable( value = "numero") String numero)
    {
        return cartaoService.ConsultarCartaoPorNumero(numero);
    }

    @GetMapping()
    Iterable<Cartao> ConsultarTodosCartoes()
    {
        return cartaoService.ConsultarTodosCartoes();
    }

    @PatchMapping("/{numero}")
    CartaoDTOEntradaStatus AtivarCartao(@PathVariable(value = "numero") String numero, @RequestBody CartaoDTOEntradaStatus cartao)
    {

        try {
            Cartao objCartao = new Cartao();
            objCartao = cartaoService.ConsultarCartaoPorNumero(numero);

            if  (cartao.getAtivo().equals("true")) {
                objCartao.setStatus(true);
            }
            else{
                objCartao.setStatus(false);
            }

            Cartao objCartaoAtivar = new Cartao();
            objCartao = cartaoService.AtivarCartao(objCartao);

            return cartao;

        }
        catch (RuntimeException ex)
        {
            throw  new   RuntimeException((ex.getMessage()));
        }
    }
}



