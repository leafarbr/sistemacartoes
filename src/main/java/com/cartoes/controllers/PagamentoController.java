package com.cartoes.controllers;

import com.cartoes.models.Cartao;
import com.cartoes.models.DTOs.PagamentoDTOEntrada;
import com.cartoes.models.Pagamento;
import com.cartoes.repository.PagamentoRepository;
import com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.jws.Oneway;
import javax.persistence.GeneratedValue;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    @GetMapping
    Iterable<Pagamento> ConsultarTodosPagamentos()
    {
        return pagamentoRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Pagamento CadastrarPagamento(@RequestBody PagamentoDTOEntrada pagamento)
    {

        try {
            Cartao objCartao = new Cartao();
            objCartao = cartaoService.ConsultarCartaoPorId(pagamento.getCarta_id());

            Pagamento objPagamento = new Pagamento(objCartao,pagamento.getDescricao(),pagamento.getValor());
            return pagamentoRepository.save(objPagamento);

        }
        catch (RuntimeException ex)
        {
            throw new RuntimeException(ex.getMessage());
        }

    }

}
