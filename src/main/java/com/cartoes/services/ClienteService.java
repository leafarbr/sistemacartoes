package com.cartoes.services;

import com.cartoes.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cartoes.repository.ClienteRepository;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente CadastraCliente(Cliente pclient)
    {
        return clienteRepository.save(pclient);
    }

    public Cliente ConsultarClientePorId(int pidcliente)
    {
        Optional<Cliente> objCliente;
        objCliente =  clienteRepository.findById(pidcliente);

        if (objCliente.isPresent())
        {
            return objCliente.get();
        }
        else
        {
            throw new RuntimeException("Usuário não encontrado na base");
        }

    }

    public Iterable<Cliente> ConsultarClientes()
    {
        return clienteRepository.findAll();
    }
}
