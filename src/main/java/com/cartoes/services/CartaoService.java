package com.cartoes.services;

import com.cartoes.models.Cartao;
import com.cartoes.models.Cliente;
import com.cartoes.repository.CartaoRepository;
import com.cartoes.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {


    @Autowired
    private CartaoRepository cartaoService;


    public Cartao CadastraCartao(Cartao pcartao)
    {
        return cartaoService.save(pcartao);
    }

    public Cartao ConsultarCartaoPorNumero(String pNumero) {

        Optional<Cartao> objCartao = cartaoService.findByNumero(pNumero);

        if (objCartao.isPresent()) {
            return objCartao.get();
        } else {
            throw new RuntimeException("Cartão não localizadao");
        }
    }

    public Cartao ConsultarCartaoPorId(int idCartao)
    {
        Optional<Cartao> objCartao = cartaoService.findById(idCartao);

        if (objCartao.isPresent())
        {
            return objCartao.get();
        }
        else
        {
            throw new RuntimeException("Cartão não localizadao");
        }
    }

    public Cartao AtivarCartao(Cartao pcartao)
    {
        return cartaoService.save((pcartao));
    }

    public Iterable<Cartao> ConsultarTodosCartoes()
    {
        return cartaoService.findAll();
    }
}
