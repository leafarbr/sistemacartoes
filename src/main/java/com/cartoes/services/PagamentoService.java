package com.cartoes.services;

import com.cartoes.models.Pagamento;
import com.cartoes.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    public Pagamento CadastrarPagamentos(Pagamento pPagamento)
    {
        return pagamentoRepository.save(pPagamento);
    }

    Iterable<Pagamento> ConsultarTodosPagamentos()
    {
        return pagamentoRepository.findAll();
    }
}
