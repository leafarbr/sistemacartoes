package com.cartoes.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String numero;

    @ManyToOne
    @NotNull
    private Cliente cliente;

    private boolean ativo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public boolean isStatus() {
        return ativo;
    }

    public void setStatus(boolean status) {
        this.ativo = status;
    }

    public Cartao() {
    }

    public Cartao(String numero, Cliente cliente, boolean ativo) {
        this.numero = numero;
        this.cliente = cliente;
        this.ativo = ativo;
    }
}
