package com.cartoes.models.DTOs;

public class CartaoDTOEntradaStatus {

  private String ativo;

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public CartaoDTOEntradaStatus() {
    }
}
