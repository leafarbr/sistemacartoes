package com.cartoes.models.DTOs;

public class PagamentoDTOEntrada {

    private int carta_id;
    private String descricao;
    private Double valor;

    public int getCarta_id() {
        return carta_id;
    }

    public void setCarta_id(int carta_id) {
        this.carta_id = carta_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
