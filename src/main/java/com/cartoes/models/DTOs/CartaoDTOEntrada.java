package com.cartoes.models.DTOs;

public class CartaoDTOEntrada {

    private String numero;
    private int clientId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
